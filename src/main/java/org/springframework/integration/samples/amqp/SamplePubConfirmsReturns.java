package org.springframework.integration.samples.amqp;

import org.apache.log4j.Logger;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author lebmax
 *
 */
public final class SamplePubConfirmsReturns {

	private static final Logger LOGGER = Logger.getLogger(SamplePubConfirmsReturns.class);

	private SamplePubConfirmsReturns() { }

	public static void main(final String... args) {

		LOGGER.info("Start exchange: ");

		@SuppressWarnings("resource")
		final AbstractApplicationContext context =
				new ClassPathXmlApplicationContext("classpath:META-INF/spring/integration/spring-integration-confirms-context.xml");

		context.registerShutdownHook();
	}
}
